﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="19008000">
	<Property Name="CCSymbols" Type="Str">CSPP_BuildContent,CSPP_Core;CSPP_WebpubLaunchBrowser,None;PPG_WithSubCycle,False;CSPP_WithDIM,False;</Property>
	<Property Name="NI.LV.All.SourceOnly" Type="Bool">true</Property>
	<Property Name="NI.Project.Description" Type="Str">This LabVIEW project "CS++.lvproj" is used to develop the successor of the CS Framework.

- CS++ will be based on native LabVIEW classes and the Actor Framework.
- CS++ will follow the KISS principle: "Keep It Smart &amp; Simple"

Please refer also to README.txt.

Author: H.Brand@gsi.de, D.Neidherr@gsi.de

Copyright 2013  GSI Helmholtzzentrum für Schwerionenforschung GmbH

Planckstr.1, 64291 Darmstadt, Germany

Lizenziert unter der EUPL, Version 1.1 oder - sobald diese von der Europäischen Kommission genehmigt wurden - Folgeversionen der EUPL ("Lizenz"); Sie dürfen dieses Werk ausschließlich gemäß dieser Lizenz nutzen.

Eine Kopie der Lizenz finden Sie hier: http://www.osor.eu/eupl

Sofern nicht durch anwendbare Rechtsvorschriften gefordert oder in schriftlicher Form vereinbart, wird die unter der Lizenz verbreitete Software "so wie sie ist", OHNE JEGLICHE GEWÄHRLEISTUNG ODER BEDINGUNGEN - ausdrücklich oder stillschweigend - verbreitet.

Die sprachspezifischen Genehmigungen und Beschränkungen unter der Lizenz sind dem Lizenztext zu entnehmen.</Property>
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Property Name="varPersistentID:{006128C9-E7F8-4B79-99C1-DA42D9CE64A4}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_4</Property>
	<Property Name="varPersistentID:{00849802-4CD5-41B9-9A62-93870E5BB25E}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Amplitude_0</Property>
	<Property Name="varPersistentID:{0220D954-7A6B-4D55-9FA5-DD314FA0A30C}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Error</Property>
	<Property Name="varPersistentID:{02D4DAB1-FA92-4613-AA34-1F166EA63443}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_Trigger_Msg</Property>
	<Property Name="varPersistentID:{03A15F9D-DC03-4FB6-A47A-F055B50C5C2E}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-Configure_Msg</Property>
	<Property Name="varPersistentID:{04E8668A-3432-4C98-B737-BA722E2736BF}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_Initialize_Msg</Property>
	<Property Name="varPersistentID:{0595C12F-463B-4C2D-A06A-334547CB541D}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_VoltageLevel_0</Property>
	<Property Name="varPersistentID:{09F550FE-B7BF-4222-996D-404C24499EDA}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Phase_0</Property>
	<Property Name="varPersistentID:{0B34EAC4-B510-4EA1-92C5-1D665DA3DE70}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Stop_Pattern_Msg</Property>
	<Property Name="varPersistentID:{0B88A466-2EBC-48A1-8330-24204879E9FD}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Frequency_0</Property>
	<Property Name="varPersistentID:{0C6BDE84-8F76-4B48-9E00-E774947A62C2}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_ResourceName</Property>
	<Property Name="varPersistentID:{0CB3A3F6-9395-4D9E-9111-5BC6689C76B2}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_7</Property>
	<Property Name="varPersistentID:{0CDB8E7E-4128-4F3D-9691-D1CBE6BC6132}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_ResourceName</Property>
	<Property Name="varPersistentID:{0D915A24-881A-40D1-BDF6-89719BDAB18D}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2Proxy_Activate</Property>
	<Property Name="varPersistentID:{0DA33869-6E90-4B4C-9D96-B17F88B8372E}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelfTest</Property>
	<Property Name="varPersistentID:{0DF30180-3E61-4804-95A5-76B196660B9F}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-Initialize_Msg</Property>
	<Property Name="varPersistentID:{0E021012-2323-4A16-9A80-C62EB5B91342}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_FirmwareRevision</Property>
	<Property Name="varPersistentID:{0E667BBD-2603-472A-8938-8E2888380D0A}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{0F0CC6AD-FBDD-4BF9-932E-8CC90BDCC987}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_OnOff_Msg</Property>
	<Property Name="varPersistentID:{0F220811-844E-4868-960C-B7B1A7AE1688}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_WaveformType_1</Property>
	<Property Name="varPersistentID:{0F9E23E0-79F8-451B-A98B-A59F000091D6}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_CurrentLimit_0</Property>
	<Property Name="varPersistentID:{10B56CC0-E25C-47BF-BBD7-7AF61CF7E311}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_NumberOfChannels</Property>
	<Property Name="varPersistentID:{130605F6-9AC7-46EB-B533-14CE99BD205F}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_9</Property>
	<Property Name="varPersistentID:{13F13F02-DE66-4E0C-8F81-AC757CD9DDE0}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_8</Property>
	<Property Name="varPersistentID:{1435EACC-ACD5-4601-A331-B9A595785089}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_DriverRevision</Property>
	<Property Name="varPersistentID:{159C80BF-8500-4AFD-B8FC-021B6C0ADCFC}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_BurstCounts_Msg</Property>
	<Property Name="varPersistentID:{16D28B5A-EAA0-4A9A-A56F-3A31D655DA87}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_ActEvent</Property>
	<Property Name="varPersistentID:{17AAD105-D027-4C93-8D2A-8DF4B4AB0116}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_MaxCurrentLimit_0</Property>
	<Property Name="varPersistentID:{190B5C95-80D6-4B57-A281-BE8695B077E6}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_7</Property>
	<Property Name="varPersistentID:{1961C32E-19E6-4528-8C8D-5E6ED8017C2C}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_DCOffset_1</Property>
	<Property Name="varPersistentID:{19738530-C8AC-4CA5-A98B-EFBA611B32BD}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_RampUpVoltage_0</Property>
	<Property Name="varPersistentID:{197883CD-0308-45F9-82BC-93D50BB88F82}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_8</Property>
	<Property Name="varPersistentID:{1998FC2C-6DAA-41A5-ACE0-C38F07380850}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingIterations</Property>
	<Property Name="varPersistentID:{19E56776-5C4D-45BB-9FB6-33F66D1F5B6B}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{1AD397CF-2FEB-4B3F-ADC8-75AC6957DB1F}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencer_guiIdString</Property>
	<Property Name="varPersistentID:{1B75C87A-2C72-40D4-8EDE-F8FCC22E6FA0}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_1</Property>
	<Property Name="varPersistentID:{1C39FDEA-800F-443B-9DEA-36EE8C44620B}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_DCOffset_Msg</Property>
	<Property Name="varPersistentID:{1CEF0EC5-59DD-41FE-AB10-9C3145793B29}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_FirmwareRevision</Property>
	<Property Name="varPersistentID:{1E5DA95F-4A47-4091-BCCD-EA7549FF082A}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_BurstMode_0</Property>
	<Property Name="varPersistentID:{200D3BB5-0048-4E34-8DCD-EB90E64A1DC4}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_DCOffset_0</Property>
	<Property Name="varPersistentID:{21175125-3F30-4F08-B4DD-A8ECF895AA4F}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Start_Pattern_Msg</Property>
	<Property Name="varPersistentID:{22669A23-2A90-41D9-B1EB-CF3A62DC13FC}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Trigger_1</Property>
	<Property Name="varPersistentID:{2279FCC2-7749-46F8-AA6E-DDB597DFE6FE}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_Amplitude_Msg</Property>
	<Property Name="varPersistentID:{22F275F7-9C45-4714-B2BC-44D45BC48CCD}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{24966D93-1C19-4B5E-830D-7F076549CBD3}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Initialized</Property>
	<Property Name="varPersistentID:{2587090C-0DF8-460B-81EA-E59B1C025C0B}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Error</Property>
	<Property Name="varPersistentID:{26D51785-0BD0-452D-B8FE-2E6F4D6B8DCD}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_Activate</Property>
	<Property Name="varPersistentID:{281AEBED-F69B-4B35-9D89-8EBC2D262ED2}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{283A42F0-6F60-4518-B555-043A5EEC1C42}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_DriverRevision</Property>
	<Property Name="varPersistentID:{28EBD3F4-27B7-4B4C-8A4D-237149A7A66A}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Configure_Output_Enable_Msg</Property>
	<Property Name="varPersistentID:{29F64853-520B-406B-938C-28B42262676C}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_2</Property>
	<Property Name="varPersistentID:{2BFDE4D9-C2B7-4CC4-94DF-C4429790E9B5}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_Trigger</Property>
	<Property Name="varPersistentID:{2CC83249-E759-4FCD-AF88-F7320F4D59B0}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Activate</Property>
	<Property Name="varPersistentID:{307123E4-C24C-4904-9054-2D87137F72D3}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_9</Property>
	<Property Name="varPersistentID:{337B6FA7-D7AC-4121-84E6-F09E146B5DB8}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_BurstCounts_0</Property>
	<Property Name="varPersistentID:{3463A663-85C0-4EF2-A47E-D2B6735ACE16}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_Activate</Property>
	<Property Name="varPersistentID:{36F88026-6934-43A9-BED7-DA7176FF5067}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Set_Letter_Time_Msg</Property>
	<Property Name="varPersistentID:{38458493-3C16-4A8D-92A7-802923A6B03F}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{3AAA41AA-FB84-4DFF-865B-1D6180983699}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_DCOffset_Msg</Property>
	<Property Name="varPersistentID:{3AC6409D-633F-43F8-8BDB-34E75BD5AFA1}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_4</Property>
	<Property Name="varPersistentID:{3AEE79D5-1BF1-424F-A6C0-9E9A54743419}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_10L</Property>
	<Property Name="varPersistentID:{3B4E5A9D-55E8-4A0B-8485-1FFDDE1277C1}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Configure_Voltage_Level_Msg</Property>
	<Property Name="varPersistentID:{3D01A9CF-558B-4ADE-8455-6DEA0791B526}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Trigger_0</Property>
	<Property Name="varPersistentID:{3D0DE57E-1986-46AA-B624-DAF25F87F7AD}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_6</Property>
	<Property Name="varPersistentID:{3D3D3C99-C583-4BE2-A8AA-5264B049B6C9}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_1L</Property>
	<Property Name="varPersistentID:{3D4BDFE5-E0C2-470A-ADBE-554B59ACE256}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_BurstCounts_0</Property>
	<Property Name="varPersistentID:{3DC99188-2E8D-4008-918B-D8731E326344}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_DriverRevision</Property>
	<Property Name="varPersistentID:{3EDD31A2-4A9E-4724-B0C6-29EB894B9318}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorStatus</Property>
	<Property Name="varPersistentID:{3F466324-E1FA-429D-87C8-DBD8651A2872}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_Activate</Property>
	<Property Name="varPersistentID:{402EC7A9-2149-4EFA-9DC4-9DF83174DA66}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Configure_NofBins_Msg</Property>
	<Property Name="varPersistentID:{40D266DA-62B7-4F62-9480-323A3BC92760}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{41FB3A4B-71DC-477D-B15D-930DC06045D3}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingInterval</Property>
	<Property Name="varPersistentID:{42E6108B-B1FC-4D0D-9C8D-9DEF682EAB16}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2Proxy_Set_OnOff_Msg</Property>
	<Property Name="varPersistentID:{447953B0-AD53-4A38-8455-7A5D72FF5D20}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_1</Property>
	<Property Name="varPersistentID:{46D7F0B1-BD22-47C1-9FDF-67480A89229C}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_Frequency_Msg</Property>
	<Property Name="varPersistentID:{478962BB-F97F-4DCC-8538-730832F5B903}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Amplitude_1</Property>
	<Property Name="varPersistentID:{47AE123F-52DB-4C24-9E17-8554C7BDB3D3}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_0H</Property>
	<Property Name="varPersistentID:{47DA957E-0720-470C-A5AB-6F657CAB1205}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_GroupStart</Property>
	<Property Name="varPersistentID:{4803E27F-2B47-4886-AA70-479539D2E972}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Error</Property>
	<Property Name="varPersistentID:{48C118DD-EE10-4D57-B75C-F30BF6614BB4}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_OnOff_Msg</Property>
	<Property Name="varPersistentID:{48E19BBB-88E8-48A2-A5FB-1A234BF89FFD}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_OnOff_1</Property>
	<Property Name="varPersistentID:{4A2A0CA7-5DBF-49CD-B928-A48C309E8212}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_2H</Property>
	<Property Name="varPersistentID:{4C567612-36C8-410D-8966-68912E5B9E45}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActorProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{4CE07ADD-49B9-4DE2-A6F3-1787336FFE42}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Configure_BinWidth_Msg</Property>
	<Property Name="varPersistentID:{4E07E57F-4657-4518-A713-C504A9269EEE}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-ResetData_Msg</Property>
	<Property Name="varPersistentID:{50B5D0AE-D899-4776-BCE8-7E88F6BA0187}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_11L</Property>
	<Property Name="varPersistentID:{50C7FA4B-13CD-460C-9B38-C5502AF93C9C}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_ResourceName</Property>
	<Property Name="varPersistentID:{5220FDF7-A048-4616-A112-9BD67D2A129A}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Configure_NofRecords_Msg</Property>
	<Property Name="varPersistentID:{533231F2-AE49-4F6E-85F6-A3D925301858}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_FirmwareRevision</Property>
	<Property Name="varPersistentID:{5336D56E-E930-4830-9AD6-72A89D381729}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Configure_Current_Limit_Msg</Property>
	<Property Name="varPersistentID:{53394B45-B007-429F-A4F4-F41B16F358D0}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_5</Property>
	<Property Name="varPersistentID:{53D4E0F4-BB7F-482C-BD76-86011632FAC2}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingTime</Property>
	<Property Name="varPersistentID:{54279077-0768-495D-876E-06D450A12996}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Start_Pattern_Msg</Property>
	<Property Name="varPersistentID:{55060716-0332-48F3-8A7A-D19D6823FA34}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_DriverRevision</Property>
	<Property Name="varPersistentID:{55B4DA02-A40A-4DE8-A50D-30F6CC577566}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingTime</Property>
	<Property Name="varPersistentID:{55EFCEB0-29E8-42C0-951F-22951A76BCCC}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_OutputEnable_2</Property>
	<Property Name="varPersistentID:{56934AF3-7224-435A-9EEA-AFE47870D1A4}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_BurstCounts_1</Property>
	<Property Name="varPersistentID:{56E9176B-7CBE-4C54-AC4B-683237815200}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Clear_Records_Msg</Property>
	<Property Name="varPersistentID:{58846B94-78CB-4F18-A6DF-65518AC13EE4}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_Measure_V_0</Property>
	<Property Name="varPersistentID:{58EFFD0F-61DD-42D3-9B15-672A2676CC07}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Set_Letter_Time_Msg</Property>
	<Property Name="varPersistentID:{59AC0BAB-AC5E-4012-84D3-EA0B916B238D}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Start_Scan_Msg</Property>
	<Property Name="varPersistentID:{5A14DEAA-07C4-44E2-A264-E3D6ABBD8D28}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_NumberOfTotalCounts</Property>
	<Property Name="varPersistentID:{5D9D3C05-73EA-4340-B90D-712882F57D23}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{5E14E013-4817-4A12-8E87-63C317571913}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_2</Property>
	<Property Name="varPersistentID:{5F56BA56-A7A1-4A92-BC78-B21CA4F88375}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_RampDownVoltage_0</Property>
	<Property Name="varPersistentID:{60191E91-EDA1-402F-A8C5-376D792E738E}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_4L</Property>
	<Property Name="varPersistentID:{6213B869-E469-4986-AC98-362303874224}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_7L</Property>
	<Property Name="varPersistentID:{62326E39-6E16-4E55-A048-9332F009BCA0}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_TDCBlocked</Property>
	<Property Name="varPersistentID:{64823389-F1E5-4CCD-9429-C205A2DF4474}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_MaxVoltageLevel_0</Property>
	<Property Name="varPersistentID:{66DB0936-A77E-41F7-983E-597D0F714D3E}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-Quit_Msg</Property>
	<Property Name="varPersistentID:{673C173C-4971-4630-8279-5823BF6FD185}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_0</Property>
	<Property Name="varPersistentID:{67DE9656-09B2-42CD-92CE-F2934748601E}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_WaveformType_Msg</Property>
	<Property Name="varPersistentID:{6854D5AC-BE9E-4617-AE78-50E50D2D4AD5}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingInterval</Property>
	<Property Name="varPersistentID:{6978FE96-B398-4049-B142-C3A6EE5AFA28}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_PGRunning</Property>
	<Property Name="varPersistentID:{69FC7BE6-048B-4458-B464-AEB61B515DE2}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_FirmwareRevision</Property>
	<Property Name="varPersistentID:{6B5FE8A0-C167-4C3C-B363-A7D29C898657}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_9L</Property>
	<Property Name="varPersistentID:{6C409B9D-745A-4E8F-BA64-8322D2797D6D}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_2L</Property>
	<Property Name="varPersistentID:{6D2B8F61-77F7-485C-B69E-B92B17775A40}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_Error</Property>
	<Property Name="varPersistentID:{6DAE975B-A5FC-4B55-8279-7BB4A2A513A3}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencer_Status</Property>
	<Property Name="varPersistentID:{6E7049C6-2FD3-40C6-AE2C-6D524BA1CEF2}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Configure_RampUpVoltage_Msg</Property>
	<Property Name="varPersistentID:{72FEF41C-384E-43A8-9275-F0038521E815}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_BinWidth</Property>
	<Property Name="varPersistentID:{73682093-DB62-4D7C-94D4-58DE23B8F08B}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Load_Pattern_Msg</Property>
	<Property Name="varPersistentID:{73C7E813-BF10-4159-89C1-31D621F3251C}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_4H</Property>
	<Property Name="varPersistentID:{748D7920-E991-4B53-982A-AF692D152BBC}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Set_Mode_Msg</Property>
	<Property Name="varPersistentID:{74C5AA27-53AE-46AA-A23D-75D179D77F68}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{74ED22F2-32F0-4844-9A4E-A2CBF6071C7A}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_8H</Property>
	<Property Name="varPersistentID:{75A890EF-A22F-4E8B-AC25-A32B59FEB49B}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2Proxy_Settings_Msg</Property>
	<Property Name="varPersistentID:{762671CC-3DB9-4E3A-BE20-D2F246A39172}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Stop_Pattern_Msg</Property>
	<Property Name="varPersistentID:{767ECDB1-16DB-4EC1-AD4E-D07D872E3B9C}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_PGRunning</Property>
	<Property Name="varPersistentID:{796908CA-F566-4886-9E2D-F3E8E9E38248}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Settings_Msg</Property>
	<Property Name="varPersistentID:{7B955735-C8C5-482E-AF6D-8361834DABFB}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManagerProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{7C4CAD0B-E29F-4ACF-8254-EBFD8C8E8D3D}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_Trigger</Property>
	<Property Name="varPersistentID:{7E9DBE39-8038-472D-A36F-1F2DD5252211}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_FirmwareRevision</Property>
	<Property Name="varPersistentID:{8000D591-723A-47BD-8E58-E1C29C334E67}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_Phase_Msg</Property>
	<Property Name="varPersistentID:{803B371E-9CC5-49C6-9BF8-27777D986A41}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_6H</Property>
	<Property Name="varPersistentID:{8055AF7F-271B-4EE9-ADBA-6EF084680E46}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Read_Data_Msg</Property>
	<Property Name="varPersistentID:{8118A2E0-1D38-48D0-B156-D49A8764BDF6}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_StopTDCs_Msg</Property>
	<Property Name="varPersistentID:{815459CE-F501-4D80-83A9-374AA2B2A049}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_PauseTDCs_Msg</Property>
	<Property Name="varPersistentID:{81E2BACB-E707-4EFC-99BB-F794C2020EC4}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Error</Property>
	<Property Name="varPersistentID:{81F9679B-25A1-4A86-8717-67C5CC80C453}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_NumberOfChannels</Property>
	<Property Name="varPersistentID:{82761AAF-74AF-4704-93B1-DA4F995A5DC4}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-StopProcess_Msg</Property>
	<Property Name="varPersistentID:{82D6C067-3ADF-48BC-BFAD-969B01FD0650}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_BurstMode_1</Property>
	<Property Name="varPersistentID:{832491A1-FA7E-42EB-9516-A52D2989CFBF}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_ResourceName</Property>
	<Property Name="varPersistentID:{8571B2B4-43A0-4F86-BEF2-9744ED4BA66B}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_3</Property>
	<Property Name="varPersistentID:{85899BA3-61C9-4E5C-B198-8A82F19064FB}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_ResourceName</Property>
	<Property Name="varPersistentID:{85C1ACCB-E477-4195-9146-FE754EC0D249}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultCode</Property>
	<Property Name="varPersistentID:{8822D1E8-A36D-4BAA-B824-E3717B606F35}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorMessage</Property>
	<Property Name="varPersistentID:{894A4118-CE7A-476C-821C-624372FB2129}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingCounter</Property>
	<Property Name="varPersistentID:{8A11D410-26F6-46E9-BCE8-500CB25A8733}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_0L</Property>
	<Property Name="varPersistentID:{8B192C52-3768-4710-B885-67CCFEEAB81A}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_Amplitude_0</Property>
	<Property Name="varPersistentID:{8C737BF7-35BB-49C3-8022-C248616BBFA0}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_2L</Property>
	<Property Name="varPersistentID:{8C879977-76B2-4EA0-A3A6-1CC428BF49CC}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_Mode</Property>
	<Property Name="varPersistentID:{8DB7C54B-8A8D-4D51-A58B-DDD22557CC23}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_Error</Property>
	<Property Name="varPersistentID:{8E3EC4A8-BF65-4F73-B83F-B97E5358483E}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Initialized</Property>
	<Property Name="varPersistentID:{907790A2-572B-47F8-8927-9AA370797AFB}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_1H</Property>
	<Property Name="varPersistentID:{9133D5DF-2DF3-4768-9BC8-724F5528C290}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Measure_Voltage_2</Property>
	<Property Name="varPersistentID:{91C2C1F5-0535-4E5A-B5EB-2E19AB729AD6}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Activate</Property>
	<Property Name="varPersistentID:{920F6BAD-9056-48F1-8297-330F09CBCE77}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingMode</Property>
	<Property Name="varPersistentID:{922786BE-9662-4B14-980F-9F192BD655BF}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_Frequency_0</Property>
	<Property Name="varPersistentID:{92C496DE-D312-4096-A7E0-D679BE3C7FF7}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_ResourceName</Property>
	<Property Name="varPersistentID:{93351BA1-15E5-4EC2-9D4A-343A1B3581FC}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_3H</Property>
	<Property Name="varPersistentID:{9426F28B-705B-4A0A-9872-FAC4F9061ACE}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Activate</Property>
	<Property Name="varPersistentID:{94E284E7-6AD6-4FD8-937A-6E8453EB6A7B}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_NumberOfRecords</Property>
	<Property Name="varPersistentID:{9509DB9A-556E-4580-B7AB-56B1F01782BC}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingDeltaT</Property>
	<Property Name="varPersistentID:{9604B0B5-B184-4FE6-9BA2-CBB6164EA850}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_Phase_0</Property>
	<Property Name="varPersistentID:{96A0D58E-145E-45D2-B871-BC4CC231A770}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorCode</Property>
	<Property Name="varPersistentID:{9869AC34-3D50-42D1-9FE8-2AD4DF4895CD}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_Status</Property>
	<Property Name="varPersistentID:{98B85454-8464-435E-92EF-0005D80030E6}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_Frequency_Msg</Property>
	<Property Name="varPersistentID:{9A43D7AE-5D0A-4D00-88CA-E005554C4B26}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_Phase_Msg</Property>
	<Property Name="varPersistentID:{9A876345-9203-48E9-B25E-C3161F16882F}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_DriverRevision</Property>
	<Property Name="varPersistentID:{9B28D4D0-1BCD-4FE9-BD4B-CA7A4E829E7F}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-StartProcess_Msg</Property>
	<Property Name="varPersistentID:{9BADD022-E038-4E2B-A488-A171B33EB2C8}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_OutputEnable_0</Property>
	<Property Name="varPersistentID:{9CB9F7FC-C4A3-4F51-BF2C-C49C93526E1F}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_3L</Property>
	<Property Name="varPersistentID:{9D72AA8E-BB10-41FD-8908-3885D6637D04}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_ResourceName</Property>
	<Property Name="varPersistentID:{9DAECC00-F378-472E-87F4-93F81EAF69FE}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Error</Property>
	<Property Name="varPersistentID:{9ECA6C91-2959-4F97-B221-344CECC2E36B}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_NumberOfCounts</Property>
	<Property Name="varPersistentID:{9EDE8AA3-3CDE-4032-A6F2-2229FEDBE9CE}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Set_Voltage_2H</Property>
	<Property Name="varPersistentID:{9F84C958-E644-4F8E-BAD9-C1EECA5D1820}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActorProxy_Activate</Property>
	<Property Name="varPersistentID:{A1785A1D-BE8E-4051-A1BF-B9201AD7959F}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_WaveformType_0</Property>
	<Property Name="varPersistentID:{A240E096-D760-4638-98EA-A547E12AEBB9}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2Proxy_Settings_Msg</Property>
	<Property Name="varPersistentID:{A29D1DAF-60ED-4786-841C-33B2AEC523FA}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{A31D70A7-7C1D-4756-AD04-96A6F17EAFBB}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Phase_1</Property>
	<Property Name="varPersistentID:{A35211C1-9086-481A-8222-26C572FEF0A6}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-PGRunning_Msg</Property>
	<Property Name="varPersistentID:{A3BF5EEC-6DD1-4015-9E0F-57FC3F9AEF17}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_1H</Property>
	<Property Name="varPersistentID:{A3C94053-9E74-451F-8584-AA2CF8F663C8}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{A432BF22-7EF4-49CE-A5CD-8FDCFE19D613}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_11</Property>
	<Property Name="varPersistentID:{A4EB0DEC-2CE3-4E5A-B00C-E26F0ABE3349}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_FirmwareRevision</Property>
	<Property Name="varPersistentID:{A5308E25-64A8-4F05-9566-3380A3C14F9B}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{A5A6987F-0756-4FCD-A911-E38E1CD59BA2}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_OutputEnable_0</Property>
	<Property Name="varPersistentID:{A64DC27C-C4CF-4E85-9D1A-8668545AD3EC}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-BreakProcess_Msg</Property>
	<Property Name="varPersistentID:{A6D33800-2F81-4827-A364-78AF0B2BC730}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_SimMode_Msg</Property>
	<Property Name="varPersistentID:{A834D626-32F0-46D1-8E96-4E4D9C0B938D}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_BurstMode_0</Property>
	<Property Name="varPersistentID:{A918DD78-984C-4B1C-BDBA-BD7C76288FF1}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ErrorStatus</Property>
	<Property Name="varPersistentID:{AABBBB62-9A5D-4914-AB0F-409124F46AD7}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2Proxy_Set_Voltage_Msg</Property>
	<Property Name="varPersistentID:{AAD57BE1-59EE-44A8-9452-904D410A5FE0}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingMode</Property>
	<Property Name="varPersistentID:{AB246FC1-9C5A-44E8-A716-926454009DB6}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Activate</Property>
	<Property Name="varPersistentID:{AC1FB631-EF68-4DC5-A7B7-F280F574466E}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_TDCPaused</Property>
	<Property Name="varPersistentID:{AD5F8123-2C10-4391-AA6F-824B82906EE0}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_10</Property>
	<Property Name="varPersistentID:{AEE6B42E-F301-4633-B3CF-8FAE2737F75B}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_NumberOfChannels</Property>
	<Property Name="varPersistentID:{B242E324-EDDA-47C0-87FE-03A313D0B08B}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_Error</Property>
	<Property Name="varPersistentID:{B25894D9-C1F7-4894-8A9A-E896CBD1B81C}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_NumberOfChannels</Property>
	<Property Name="varPersistentID:{B25A14D0-F896-41E6-BF14-660C15F31FBE}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_Frequency_1</Property>
	<Property Name="varPersistentID:{B2651DE0-4704-45BF-8D36-DB62DD686E65}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_Trigger_Msg</Property>
	<Property Name="varPersistentID:{B2ECD390-1AF7-4CF4-ABF1-770640581F46}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_10</Property>
	<Property Name="varPersistentID:{B58EF376-56A7-414F-8C01-BA7174DA6DA9}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_6L</Property>
	<Property Name="varPersistentID:{B663E0A6-8FCD-4D18-8D14-4888140C6380}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_0</Property>
	<Property Name="varPersistentID:{B81D4C02-7C00-4A23-BE1E-220804DFCDB7}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Measure_Voltage_0</Property>
	<Property Name="varPersistentID:{B8C13779-82EC-4364-9576-EBA1F9DB881E}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_Activate</Property>
	<Property Name="varPersistentID:{B8EC207E-D443-43F2-9413-59CB811D7245}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_DriverRevision</Property>
	<Property Name="varPersistentID:{BA1CC485-2078-45D4-8350-DB47C1120925}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{BB30BA1C-597C-4614-B292-196404B36E85}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-Ping_Msg</Property>
	<Property Name="varPersistentID:{BC60042C-2B90-4DBF-8BA9-67A4E70045B1}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Error</Property>
	<Property Name="varPersistentID:{BD5E43C6-0B76-43A4-B500-6B217876B3FA}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_ResourceName</Property>
	<Property Name="varPersistentID:{BE8379ED-F4CD-42DD-A46B-87E829F8D270}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_FirmwareRevision</Property>
	<Property Name="varPersistentID:{BEA1CE07-7107-4B78-AAC6-0681EB96CB90}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_FirmwareRevision</Property>
	<Property Name="varPersistentID:{BFE093DC-B615-43C9-B46E-59C976B48294}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_10H</Property>
	<Property Name="varPersistentID:{C026CBD7-F3AB-4806-B63D-120D0BA8B009}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingInterval</Property>
	<Property Name="varPersistentID:{C09C237D-0938-4912-83A0-204D4251B308}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_WaveformType_Msg</Property>
	<Property Name="varPersistentID:{C1E6ABF1-8AC7-444A-8B76-D9C463B058B6}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{C22D936A-A5D9-4BAC-BE65-6F145F77B093}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_BurstMode_Msg</Property>
	<Property Name="varPersistentID:{C2D5A0E9-CA59-4E70-80B6-19B08D487DA9}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2Proxy_WorkerActor</Property>
	<Property Name="varPersistentID:{C30A9014-FA87-4A75-8D95-A1C437D72966}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Load_Pattern_Msg</Property>
	<Property Name="varPersistentID:{C34DB6ED-5333-45B8-B9F3-F059BBDA3DE1}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_Configure_RampDownVoltage_Msg</Property>
	<Property Name="varPersistentID:{C458EC3E-C31C-4BFD-8DD2-31A9C325079E}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_Amplitude_Msg</Property>
	<Property Name="varPersistentID:{C58FB1CD-D9FD-4B9C-BC78-65F373A2723B}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Reset_Device_Msg</Property>
	<Property Name="varPersistentID:{C5DC1C8F-31FD-4140-AE3C-3C038684B4F8}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Set-PollingIterations</Property>
	<Property Name="varPersistentID:{C6184893-EFB8-4E8E-B50E-A5C2E3FF6F0E}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_OnOff_0</Property>
	<Property Name="varPersistentID:{C8FA485C-4EE9-424D-8EAC-2BD78D658801}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_OnOff_0</Property>
	<Property Name="varPersistentID:{CABC9D9F-F08D-41B5-81A0-3AAAFCF1380E}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HPProxy_StartTDCs_Msg</Property>
	<Property Name="varPersistentID:{CB536BFC-73B5-43E2-A060-B5D981E53BB5}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_Get_DCOffset_0</Property>
	<Property Name="varPersistentID:{CB72173A-46D5-4849-8B6C-386058DE902E}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2Proxy_Activate</Property>
	<Property Name="varPersistentID:{CBF24FF2-D6F3-43A1-BEA9-BF8B48294845}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_Reset</Property>
	<Property Name="varPersistentID:{CDE3383E-4CA0-46AB-BE68-91816F31F8FA}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Set_Mode_Msg</Property>
	<Property Name="varPersistentID:{CE594826-0EBE-411A-955C-F114F2255C48}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorCode</Property>
	<Property Name="varPersistentID:{D00F4410-09A0-4BFA-9D5B-9608679DAFB4}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_Set-PollingStartStop</Property>
	<Property Name="varPersistentID:{D0522C77-2B44-49BF-A393-4C10EDCFF3C3}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_5</Property>
	<Property Name="varPersistentID:{D0A701F5-C7BD-4FE6-8632-0E13378BDF8F}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_8L</Property>
	<Property Name="varPersistentID:{D1D3A81A-0D1B-4CAF-A422-5D8CABF72C24}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_ErrorCode</Property>
	<Property Name="varPersistentID:{D20F200A-9A64-4344-A178-39A1280985A5}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPGProxy_Activate</Property>
	<Property Name="varPersistentID:{D33DE266-D035-48B6-83FD-0B028C70183B}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Set_BurstCounts_Msg</Property>
	<Property Name="varPersistentID:{D493AE39-9ACF-4C1E-A235-7EA34956C76D}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_0L</Property>
	<Property Name="varPersistentID:{D58B5CEC-92C0-471A-B431-474EBDF4BE76}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_FirmwareRevision</Property>
	<Property Name="varPersistentID:{D6490280-7D76-468F-ABC2-DBFECF3EFB81}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_Settings_Msg</Property>
	<Property Name="varPersistentID:{D756B451-47B2-4607-B06D-6BD26C206FB3}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXXProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{D7C78A74-A245-462D-AE82-66DFC06639C3}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_7H</Property>
	<Property Name="varPersistentID:{D913CA2A-B3D9-47F6-8F47-146A7644FD48}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_SimulationMode</Property>
	<Property Name="varPersistentID:{DABFD4CB-58A3-4E68-A902-6B40EBCA4DDE}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Activate</Property>
	<Property Name="varPersistentID:{DB7BD259-F08A-4275-856E-C074B93E9ABE}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingIterations</Property>
	<Property Name="varPersistentID:{DB7CA0D6-A654-4516-9C92-D28C01E4E298}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Measure_Voltage_3</Property>
	<Property Name="varPersistentID:{DC1B404A-6423-47CE-A74B-34CD6599B90C}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_NumberOfBins</Property>
	<Property Name="varPersistentID:{DCEADFC5-D1FB-45C9-B20E-F53B9FB357C2}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent33XXX_SV.lvlib/myAgilent33XXX_Get_WaveformType_0</Property>
	<Property Name="varPersistentID:{DD3EF561-F1E8-4784-BE65-080A6A5E3636}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_ErrorMessage</Property>
	<Property Name="varPersistentID:{DD9E6071-BF5C-402A-B079-25ECDF84EA7F}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingIterations</Property>
	<Property Name="varPersistentID:{DE30B52C-4DA9-404A-9EF0-04565DF5432A}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_Error</Property>
	<Property Name="varPersistentID:{DECB6A4B-E8CB-4DF2-AD3E-222320D729A8}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_Measure_Voltage_1</Property>
	<Property Name="varPersistentID:{DED744C5-310F-4214-BFB0-D7350E10C177}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_DriverRevision</Property>
	<Property Name="varPersistentID:{E00610EB-3C76-4BFE-8AC8-6B25F0C16CDD}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_0H</Property>
	<Property Name="varPersistentID:{E0996583-B607-4E8B-BFFC-9A4E67C1EC73}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_5H</Property>
	<Property Name="varPersistentID:{E3567B6A-C05D-4D99-804A-173C0F7B8143}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2_OutputEnable_1</Property>
	<Property Name="varPersistentID:{E3B9465F-7075-460A-B1C7-56D65A079E85}" Type="Ref">/My Computer/Packages/PowerSupply/SV/GSI_HVSwitch2_ExampleSVs.lvlib/myGSI-HVSwitch2Proxy_Set_Voltage_Msg</Property>
	<Property Name="varPersistentID:{E586605E-D14C-470A-BCE6-591140D9A049}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_NumberOfChannels</Property>
	<Property Name="varPersistentID:{E65C4E86-B786-4B44-85F8-8E1C2D75469D}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_6</Property>
	<Property Name="varPersistentID:{E68AC498-491F-4ACE-9381-444D46AC0FB0}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_ActWaveform</Property>
	<Property Name="varPersistentID:{E7D6C7F2-50E9-4872-AB8E-F520D9E8033D}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAENProxy_WorkerActor</Property>
	<Property Name="varPersistentID:{E85C665B-570D-4AAF-BDA8-404BF980FAEB}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCS_Error</Property>
	<Property Name="varPersistentID:{E87716FF-62B5-420B-BAD3-68F8CD5FF2C0}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352X_DriverRevision</Property>
	<Property Name="varPersistentID:{E89AC484-97D2-4540-AB95-F55F7632289A}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_SelftestResultCode</Property>
	<Property Name="varPersistentID:{E8AA67A9-FB0D-46C5-987E-7547920F26B8}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_ResourceName</Property>
	<Property Name="varPersistentID:{E9D8F113-2120-4CF8-B39E-C374997636F5}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-PPG_Error</Property>
	<Property Name="varPersistentID:{ECB775BD-FF75-4C61-AD56-BF93044F5118}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingTime</Property>
	<Property Name="varPersistentID:{ED97E654-3439-4617-95C5-3004D0BCE4AE}" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib/myFPGA-PPG_Mode</Property>
	<Property Name="varPersistentID:{EDBF969C-FCDA-4666-97BE-DC63CAC2169E}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_9H</Property>
	<Property Name="varPersistentID:{EDEBF2BC-C0CD-4816-9548-E557FDFA5239}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2Proxy_Set_OnOff_Msg</Property>
	<Property Name="varPersistentID:{EEC870D3-3EC4-48B6-A085-A7E76113DD0B}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_GroupStop</Property>
	<Property Name="varPersistentID:{EF9BC677-195F-4AAB-BA01-F5CCB89E4909}" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib/ObjectManager_PollingCounter</Property>
	<Property Name="varPersistentID:{F0202B4B-434F-4DF4-B3D6-2CA57E6B5FC7}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_SelftestResultMessage</Property>
	<Property Name="varPersistentID:{F0BA504C-09D6-4380-88C3-92F4C4273859}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/BaseActor_PollingInterval</Property>
	<Property Name="varPersistentID:{F271F88C-A42F-4162-B184-4535D9EDFF2A}" Type="Ref">/My Computer/Packages/FGen/SV/Agilent3352X_SV.lvlib/myAgilent3352XProxy_Set_BurstMode_Msg</Property>
	<Property Name="varPersistentID:{F37D72B8-ED3F-4E68-B083-E5C8C7628A08}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingDeltaT</Property>
	<Property Name="varPersistentID:{F3C078F4-95D3-46A4-9B49-08906CA93217}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingCounter</Property>
	<Property Name="varPersistentID:{F69083F6-9074-4846-9FA8-C81C2F297FBA}" Type="Ref">/My Computer/Packages/PowerSupply/SV/CAEN_ExampleSVs.lvlib/myCAEN_Measure_A_0</Property>
	<Property Name="varPersistentID:{F71851FB-76C8-41A5-8371-FFEFB9715273}" Type="Ref">/My Computer/Packages/MM/CSPP_MM_SV.lvlib/myMMSequencerProxy_MM-CycleFinished_Msg</Property>
	<Property Name="varPersistentID:{F8101550-A0BE-45B0-B520-34CE8AF7EE1C}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_5L</Property>
	<Property Name="varPersistentID:{F852394B-17B2-42C1-A7C0-195440F77E41}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_11H</Property>
	<Property Name="varPersistentID:{FB228133-7303-4FE7-8F17-21DD31640176}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_Set_Voltage_1L</Property>
	<Property Name="varPersistentID:{FC89F439-8732-4842-8EFF-DF06D63D7877}" Type="Ref">/My Computer/Packages/FPGA/CSPP_FPGA_SV.lvlib/myFPGA-MCSProxy_Stop_Scan_Msg</Property>
	<Property Name="varPersistentID:{FD8C6EFF-80F2-40BE-BBF4-8665E3540D90}" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib/DeviceActor_PollingMode</Property>
	<Property Name="varPersistentID:{FF37BAE3-5BAF-4D0F-A49B-FDB89B6C2208}" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib/myGSI-HVSwitch2_OutputEnable_11</Property>
	<Property Name="varPersistentID:{FF4D28B5-589F-48DF-ACC3-2AC1CF718158}" Type="Ref">/My Computer/Packages/Acquisition/CSPP_Acquisition_SV.lvlib/myTDC8HP_TDCStarted</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="_Applikation Contents-Files" Type="Folder">
			<Item Name="CSPP_Genesys.vi" Type="VI" URL="../_Applikation Contents-Files/CSPP_Genesys.vi"/>
			<Item Name="CSPP_HVSwitch2.vi" Type="VI" URL="../_Applikation Contents-Files/CSPP_HVSwitch2.vi"/>
			<Item Name="CSPP_ISEG.vi" Type="VI" URL="../_Applikation Contents-Files/CSPP_ISEG.vi"/>
			<Item Name="CSPP_PPG.vi" Type="VI" URL="../_Applikation Contents-Files/CSPP_PPG.vi"/>
		</Item>
		<Item Name="_Applikation Ini-Files" Type="Folder">
			<Item Name="CSPP_Genesys.ini" Type="Document" URL="../_Applikation Ini Files/CSPP_Genesys.ini"/>
			<Item Name="CSPP_HVSwitch2.ini" Type="Document" URL="../_Applikation Ini Files/CSPP_HVSwitch2.ini"/>
			<Item Name="CSPP_ISEG.ini" Type="Document" URL="../_Applikation Ini Files/CSPP_ISEG.ini"/>
			<Item Name="CSPP_PPG.ini" Type="Document" URL="../_Applikation Ini Files/CSPP_PPG.ini"/>
		</Item>
		<Item Name="_Applikation SV-Files" Type="Folder">
			<Item Name="myCSPP_FPGA_SV.lvlib" Type="Library" URL="../_Applikation SV-Files/myCSPP_FPGA_SV.lvlib"/>
			<Item Name="myGSI-HVSwitch2.lvlib" Type="Library" URL="../_Applikation SV-Files/myGSI-HVSwitch2.lvlib"/>
		</Item>
		<Item Name="AF" Type="Folder">
			<Property Name="NI.SortType" Type="Int">0</Property>
			<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="Report Error Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Report Error Msg/Report Error Msg.lvclass"/>
			<Item Name="Self-Addressed Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Self-Addressed Msg/Self-Addressed Msg.lvclass"/>
		</Item>
		<Item Name="Documentation" Type="Folder">
			<Item Name="archsp.doc" Type="Document" URL="../Documentation/archsp.doc"/>
			<Item Name="CS++@VIP2013.pptx" Type="Document" URL="../Documentation/CS++@VIP2013.pptx"/>
			<Item Name="detspec.doc" Type="Document" URL="../Documentation/detspec.doc"/>
			<Item Name="Github Checklist.txt" Type="Document" URL="../Documentation/Github Checklist.txt"/>
			<Item Name="swreqsp.doc" Type="Document" URL="../Documentation/swreqsp.doc"/>
			<Item Name="userman.doc" Type="Document" URL="../Documentation/userman.doc"/>
			<Item Name="usreqsp.doc" Type="Document" URL="../Documentation/usreqsp.doc"/>
		</Item>
		<Item Name="EUPL License" Type="Folder">
			<Item Name="EUPL v.1.1 - Lizenz.pdf" Type="Document" URL="../EUPL v.1.1 - Lizenz.pdf"/>
			<Item Name="EUPL v.1.1 - Lizenz.rtf" Type="Document" URL="../EUPL v.1.1 - Lizenz.rtf"/>
		</Item>
		<Item Name="instr.lib" Type="Folder">
			<Item Name="Agilent 33XXX Series.lvlib" Type="Library" URL="../instr.lib/Agilent33XXX/Agilent 33XXX Series.lvlib"/>
			<Item Name="Agilent 3352X Series.lvlib" Type="Library" URL="../instr.lib/Agilent3352X/Agilent 3352X Series.lvlib"/>
			<Item Name="DS345Driver.lvlib" Type="Library" URL="../instr.lib/DS345/DS345Driver.lvlib"/>
			<Item Name="GENserDriver.lvlib" Type="Library" URL="../instr.lib/GENser/GENserDriver.lvlib"/>
			<Item Name="HD-Switch.lvlib" Type="Library" URL="../instr.lib/HD-Switch/HD-Switch.lvlib"/>
			<Item Name="HVSwitch2Driver.lvlib" Type="Library" URL="../instr.lib/HVSwitch2/HVSwitch2Driver.lvlib"/>
			<Item Name="NI-FPGA-MCS_Driver.lvlib" Type="Library" URL="../instr.lib/NI-FPGA-MCS/host/instrumentDriver/NI-FPGA-MCS_Driver.lvlib"/>
			<Item Name="NI-FPGA-PPG_Driver.lvlib" Type="Library" URL="../instr.lib/NI-FPGA-PPG/host/instrumentDriver/NI-FPGA-PPG_Driver.lvlib"/>
			<Item Name="SDEXDriver.lvlib" Type="Library" URL="../instr.lib/SDEX/SDEXDriver.lvlib"/>
			<Item Name="TDC8HP.lvlib" Type="Library" URL="../instr.lib/TDC8HP/TDC8HP.lvlib"/>
		</Item>
		<Item Name="Libraries" Type="Folder">
			<Item Name="DimIndicators.lvlib" Type="Library" URL="../Packages/DimLVEvent/DimIndicators/DimIndicators.lvlib"/>
			<Item Name="LVDimInterface.lvlib" Type="Library" URL="../Packages/DimLVEvent/LVDimInterface/LVDimInterface.lvlib"/>
		</Item>
		<Item Name="Packages" Type="Folder">
			<Item Name="Acquisition" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_TDC8HP.lvlib" Type="Library" URL="../Packages/CSPP_Acquisition/Actors/CSPP_TDC8HP/CSPP_TDC8HP.lvlib"/>
					<Item Name="CSPP_TDC8HPGui.lvlib" Type="Library" URL="../Packages/CSPP_Acquisition/Actors/CSPP_TDC8HPGui/CSPP_TDC8HPGui.lvlib"/>
				</Item>
				<Item Name="CSPP_Acquisition_SV.lvlib" Type="Library" URL="../Packages/CSPP_Acquisition/CSPP_Acquisition_SV.lvlib"/>
				<Item Name="CSPP_Acquisition-Content.vi" Type="VI" URL="../Packages/CSPP_Acquisition/CSPP_Acquisition-Content.vi"/>
				<Item Name="CSPP_Acquisition.ini" Type="Document" URL="../Packages/CSPP_Acquisition/CSPP_Acquisition.ini"/>
			</Item>
			<Item Name="Core" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_BaseActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_BaseActor/CSPP_BaseActor.lvlib"/>
					<Item Name="CSPP_DeviceActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceActor/CSPP_DeviceActor.lvlib"/>
					<Item Name="CSPP_DeviceGUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DeviceGUIActor/CSPP_DeviceGUIActor.lvlib"/>
					<Item Name="CSPP_DSMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_DSMonitor/CSPP_DSMonitor.lvlib"/>
					<Item Name="CSPP_GUIActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_GUIActor/CSPP_GUIActor.lvlib"/>
					<Item Name="CSPP_LMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LMMonitor/CSPP_LMMonitor.lvlib"/>
					<Item Name="CSPP_LNMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_LNMonitor/CSPP_LNMonitor.lvlib"/>
					<Item Name="CSPP_PVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVMonitor/CSPP_PVMonitor.lvlib"/>
					<Item Name="CSPP_PVProxy.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVProxy/CSPP_PVProxy.lvlib"/>
					<Item Name="CSPP_StartActor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_StartActor/CSPP_StartActor.lvlib"/>
					<Item Name="CSPP_SVMonitor.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_SVMonitor/CSPP_SVMonitor.lvlib"/>
					<Item Name="CSPP_TDMSStorage.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_TDMSStorage/CSPP_TDMSStorage.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_BaseClasses.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_BaseClasses/CSPP_BaseClasses.lvlib"/>
					<Item Name="CSPP_ProcessVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/CSPP_ProcessVariables.lvlib"/>
					<Item Name="CSPP_SharedVariables.lvlib" Type="Library" URL="../Packages/CSPP_Core/Classes/CSPP_ProcessVariables/SVConnection/CSPP_SharedVariables.lvlib"/>
				</Item>
				<Item Name="Documentation" Type="Folder">
					<Item Name="Change_Log.txt" Type="Document" URL="../Packages/CSPP_Core/Change_Log.txt"/>
					<Item Name="Release_Notes.txt" Type="Document" URL="../Packages/CSPP_Core/Release_Notes.txt"/>
					<Item Name="VI-Analyzer-Configuration.cfg" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Configuration.cfg"/>
					<Item Name="VI-Analyzer-Results.rsl" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Results.rsl"/>
					<Item Name="VI-Analyzer-Spelling-Exceptions.txt" Type="Document" URL="../Packages/CSPP_Core/VI-Analyzer-Spelling-Exceptions.txt"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_Base.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Base/CSPP_Base.lvlib"/>
					<Item Name="CSPP_Utilities.lvlib" Type="Library" URL="../Packages/CSPP_Core/Libraries/Utilities/CSPP_Utilities.lvlib"/>
				</Item>
				<Item Name="Messages" Type="Folder">
					<Property Name="NI.SortType" Type="Int">0</Property>
					<Item Name="CSPP_AEUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AEUpdate Msg/CSPP_AEUpdate Msg.lvlib"/>
					<Item Name="CSPP_AsyncCallbackMsg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_AsyncCallbackMsg/CSPP_AsyncCallbackMsg.lvlib"/>
					<Item Name="CSPP_DataUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_DataUpdate Msg/CSPP_DataUpdate Msg.lvlib"/>
					<Item Name="CSPP_NAInitialized Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_NAInitialized Msg/CSPP_NAInitialized Msg.lvlib"/>
					<Item Name="CSPP_PVUpdate Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_PVUpdate Msg/CSPP_PVUpdate Msg.lvlib"/>
				</Item>
				<Item Name="CSPP_Core.ini" Type="Document" URL="../Packages/CSPP_Core/CSPP_Core.ini"/>
				<Item Name="CSPP_Core_SV.lvlib" Type="Library" URL="../Packages/CSPP_Core/CSPP_Core_SV.lvlib"/>
				<Item Name="CSPP_CoreContent-Linux.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent-Linux.vi"/>
				<Item Name="CSPP_CoreContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreContent.vi"/>
				<Item Name="CSPP_CoreGUIContent.vi" Type="VI" URL="../Packages/CSPP_Core/CSPP_CoreGUIContent.vi"/>
			</Item>
			<Item Name="DeviceBase" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_DCPwr.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DCPwr/CSPP_DCPwr.lvlib"/>
					<Item Name="CSPP_DCPwrGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DCPwrGui/CSPP_DCPwrGui.lvlib"/>
					<Item Name="CSPP_FGen.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_FGen/CSPP_FGen.lvlib"/>
					<Item Name="CSPP_FGenGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_FGenGui/CSPP_FGenGui.lvlib"/>
					<Item Name="CSPP_MCS.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_MCS/CSPP_MCS.lvlib"/>
					<Item Name="CSPP_MCSGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_MCSGui/CSPP_MCSGui.lvlib"/>
					<Item Name="CSPP_Motor.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_Motor/CSPP_Motor.lvlib"/>
					<Item Name="CSPP_MotorGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_MotorGui/CSPP_MotorGui.lvlib"/>
					<Item Name="CSPP_PPG.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_PPG/CSPP_PPG.lvlib"/>
					<Item Name="CSPP_PPGGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_PPGGui/CSPP_PPGGui.lvlib"/>
					<Item Name="CSPP_Scope.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_Scope/CSPP_Scope.lvlib"/>
					<Item Name="CSPP_ScopeGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_ScopeGui/CSPP_ScopeGui.lvlib"/>
				</Item>
				<Item Name="CSPP_DeviceBase-Content.vi" Type="VI" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBase-Content.vi"/>
				<Item Name="CSPP_DeviceBase.ini" Type="Document" URL="../Packages/CSPP_DeviceBase/CSPP_DeviceBase.ini"/>
			</Item>
			<Item Name="DIM" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CS_CommandMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DIM/CS_CommandMonitor/CS_CommandMonitor.lvlib"/>
					<Item Name="CSPP_DIMMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DIM/DIM_Monitor/CSPP_DIMMonitor.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_CS.lvlib" Type="Library" URL="../Packages/CSPP_DIM/CSConnection/CSPP_CS.lvlib"/>
					<Item Name="CSPP_DIM.lvlib" Type="Library" URL="../Packages/CSPP_DIM/DIMConnection/CSPP_DIM.lvlib"/>
				</Item>
				<Item Name="Examples" Type="Folder">
					<Item Name="CSPP_DIM_PerfClient.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_PerfClient/CSPP_DIM_PerfClient.lvlib"/>
					<Item Name="CSPP_DIM_PerfServer.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_PerfServer/CSPP_DIM_PerfServer.lvlib"/>
					<Item Name="CSPP_DIM_TypeServer.lvlib" Type="Library" URL="../Packages/CSPP_DIM/Examples/CSPP_DIM_TypeServer/CSPP_DIM_TypeServer.lvlib"/>
				</Item>
				<Item Name="TestVIs" Type="Folder">
					<Item Name="CSClusterTest.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSClusterTest.vi"/>
					<Item Name="CSClusterTest2.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSClusterTest2.vi"/>
					<Item Name="CSClusterTest3.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSClusterTest3.vi"/>
					<Item Name="CSClusterTest4.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSClusterTest4.vi"/>
					<Item Name="CSClusterTest5.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSClusterTest5.vi"/>
					<Item Name="CSCommandTest.vi" Type="VI" URL="../Packages/CSPP_DIM/TestVIs/CSCommandTest.vi"/>
				</Item>
				<Item Name="CSPP_DIM-errors.txt" Type="Document" URL="../Packages/CSPP_DIM/CSPP_DIM-errors.txt"/>
				<Item Name="CSPP_DIM.ini" Type="Document" URL="../Packages/CSPP_DIM/CSPP_DIM.ini"/>
				<Item Name="CSPP_DIMContent.vi" Type="VI" URL="../Packages/CSPP_DIM/CSPP_DIMContent.vi"/>
				<Item Name="DIM Type Descriptors.docx" Type="Document" URL="../Packages/CSPP_DIM/DIM Type Descriptors.docx"/>
			</Item>
			<Item Name="DSC" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_DSCAlarmViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCAlarmViewer/CSPP_DSCAlarmViewer.lvlib"/>
					<Item Name="CSPP_DSCManager.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCManager/CSPP_DSCManager.lvlib"/>
					<Item Name="CSPP_DSCMonitor.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCMonitor/CSPP_DSCMonitor.lvlib"/>
					<Item Name="CSPP_DSCMsgLogger.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/CSPP_DSCMsgLogger/CSPP_DSCMsgLogger.lvlib"/>
					<Item Name="CSPP_DSCTrendViewer.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Actors/CSPP_DSCTrendViewer/CSPP_DSCTrendViewer.lvlib"/>
				</Item>
				<Item Name="Classes" Type="Folder">
					<Item Name="CSPP_DSCConnection.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Classes/DSCConnection/CSPP_DSCConnection.lvlib"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="CSPP_DSCUtilities.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Libs/CSPP_DSCUtilities/CSPP_DSCUtilities.lvlib"/>
					<Item Name="DSC Remote SV Access.lvlib" Type="Library" URL="../Packages/CSPP_DSC/Contributed/DSC Remote SV Access.lvlib"/>
				</Item>
				<Item Name="CSPP_DSC.ini" Type="Document" URL="../Packages/CSPP_DSC/CSPP_DSC.ini"/>
				<Item Name="CSPP_DSCContent.vi" Type="VI" URL="../Packages/CSPP_DSC/CSPP_DSCContent.vi"/>
			</Item>
			<Item Name="FGen" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="Agilent33XXX.lvlib" Type="Library" URL="../Packages/CSPP_FGen/Actors/Agilent33XXX/Agilent33XXX.lvlib"/>
					<Item Name="Agilent3352X.lvlib" Type="Library" URL="../Packages/CSPP_FGen/Actors/Agilent3352X/Agilent3352X.lvlib"/>
				</Item>
				<Item Name="SV" Type="Folder">
					<Item Name="Agilent33XXX_SV.lvlib" Type="Library" URL="../Packages/CSPP_FGen/SV/Agilent33XXX_SV.lvlib"/>
					<Item Name="Agilent3352X_SV.lvlib" Type="Library" URL="../Packages/CSPP_FGen/SV/Agilent3352X_SV.lvlib"/>
				</Item>
				<Item Name="CSPP_FGen-Content.vi" Type="VI" URL="../Packages/CSPP_FGen/CSPP_FGen-Content.vi"/>
				<Item Name="CSPP_FGen.ini" Type="Document" URL="../Packages/CSPP_FGen/CSPP_FGen.ini"/>
			</Item>
			<Item Name="FPGA" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_FPGA-MCS.lvlib" Type="Library" URL="../Packages/CSPP_FPGA/Actors/CSPP_FPGA-MCS/CSPP_FPGA-MCS.lvlib"/>
					<Item Name="CSPP_FPGA-PPG.lvlib" Type="Library" URL="../Packages/CSPP_FPGA/Actors/CSPP_FPGA-PPG/CSPP_FPGA-PPG.lvlib"/>
				</Item>
				<Item Name="CSPP_FPGA.ini" Type="Document" URL="../Packages/CSPP_FPGA/CSPP_FPGA.ini"/>
				<Item Name="CSPP_FPGA_SV.lvlib" Type="Library" URL="../Packages/CSPP_FPGA/CSPP_FPGA_SV.lvlib"/>
				<Item Name="CSPP_FPGA-Content.vi" Type="VI" URL="../Packages/CSPP_FPGA/CSPP_FPGA-Content.vi"/>
			</Item>
			<Item Name="ListGUI" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_ListGui.lvlib" Type="Library" URL="../Packages/CSPP_ListGUI/Actors/CSPP_ListGUI/CSPP_ListGui.lvlib"/>
				</Item>
				<Item Name="CSPP_ListGui-Content.vi" Type="VI" URL="../Packages/CSPP_ListGUI/CSPP_ListGui-Content.vi"/>
			</Item>
			<Item Name="MM" Type="Folder">
				<Property Name="NI.SortType" Type="Int">3</Property>
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_MMArchiver.lvlib" Type="Library" URL="../Packages/CSPP_MM/Actors/CSPP_MMArchiver/CSPP_MMArchiver.lvlib"/>
					<Item Name="CSPP_MMEventBuilder.lvlib" Type="Library" URL="../Packages/CSPP_MM/Actors/CSPP_MMEventBuilder/CSPP_MMEventBuilder.lvlib"/>
					<Item Name="CSPP_MMSequencer.lvlib" Type="Library" URL="../Packages/CSPP_MM/Actors/CSPP_MMSequencer/CSPP_MMSequencer.lvlib"/>
					<Item Name="CSPP_MMSequencerGUI.lvlib" Type="Library" URL="../Packages/CSPP_MM/Actors/CSPP_MMSequencerGUI/CSPP_MMSequencerGUI.lvlib"/>
				</Item>
				<Item Name="Libraries" Type="Folder">
					<Item Name="LEBITDefs.lvlib" Type="Library" URL="../Packages/CSPP_MM/Libraries/LEBITDefs/LEBITDefs.lvlib"/>
					<Item Name="LEBITLib.lvlib" Type="Library" URL="../Packages/CSPP_MM/Libraries/LEBITLib/LEBITLib.lvlib"/>
					<Item Name="MMDataLib.lvlib" Type="Library" URL="../Packages/CSPP_MM/Libraries/MMDataLib/MMDataLib.lvlib"/>
				</Item>
				<Item Name="CSPP_MM_SV.lvlib" Type="Library" URL="../Packages/CSPP_MM/CSPP_MM_SV.lvlib"/>
				<Item Name="CSPP_MM-Content.vi" Type="VI" URL="../Packages/CSPP_MM/CSPP_MM-Content.vi"/>
				<Item Name="CSPP_MM.ini" Type="Document" URL="../Packages/CSPP_MM/CSPP_MM.ini"/>
			</Item>
			<Item Name="ObjectManager" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CSPP_ObjectManager.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.lvlib"/>
				</Item>
				<Item Name="CSPP_ObjectManager.ini" Type="Document" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager.ini"/>
				<Item Name="CSPP_ObjectManager_Content.vi" Type="VI" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_Content.vi"/>
				<Item Name="CSPP_ObjectManager_SV.lvlib" Type="Library" URL="../Packages/CSPP_ObjectManager/CSPP_ObjectManager_SV.lvlib"/>
			</Item>
			<Item Name="PowerSupply" Type="Folder">
				<Item Name="Actors" Type="Folder">
					<Item Name="CAEN.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/CAEN/CAEN.lvlib"/>
					<Item Name="GSI-HVSwitch2.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/GSI-HVSwitch2/GSI-HVSwitch2.lvlib"/>
					<Item Name="HD-HVSwitch.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/HD-HVSwitch/HD-HVSwitch.lvlib"/>
					<Item Name="ISEG.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/ISEG/ISEG.lvlib"/>
					<Item Name="LambdaGenesys.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/LambdaGenesys/LambdaGenesys.lvlib"/>
					<Item Name="PSConnect.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/Actors/PSConnect/PSConnect.lvlib"/>
				</Item>
				<Item Name="SV" Type="Folder">
					<Item Name="CAEN_ExampleSVs.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/SV_Configurations/CAEN_ExampleSVs.lvlib"/>
					<Item Name="GSI_HVSwitch2_ExampleSVs.lvlib" Type="Library" URL="../Packages/CSPP_PowerSupply/SV_Configurations/GSI_HVSwitch2_ExampleSVs.lvlib"/>
				</Item>
				<Item Name="CSPP_PowerSupply-Content.vi" Type="VI" URL="../Packages/CSPP_PowerSupply/CSPP_PowerSupply-Content.vi"/>
				<Item Name="CSPP_PowerSupply.ini" Type="Document" URL="../Packages/CSPP_PowerSupply/CSPP_PowerSupply.ini"/>
			</Item>
		</Item>
		<Item Name="User" Type="Folder">
			<Item Name="DIMTest.lvlib" Type="Library" URL="../User/Actors/DIMTest/DIMTest.lvlib"/>
			<Item Name="PPGTest.lvlib" Type="Library" URL="../User/Actors/PPGTest/PPGTest.lvlib"/>
			<Item Name="User-Content.vi" Type="VI" URL="../User/User-Content.vi"/>
		</Item>
		<Item Name="Change_Log.txt" Type="Document" URL="../Change_Log.txt"/>
		<Item Name="CSPP.ico" Type="Document" URL="../CSPP.ico"/>
		<Item Name="CSPP.ini" Type="Document" URL="../CSPP.ini"/>
		<Item Name="CSPP_Main.vi" Type="VI" URL="../CSPP_Main.vi"/>
		<Item Name="CSPP_MainExe.vi" Type="VI" URL="../CSPP_MainExe.vi"/>
		<Item Name="README.md" Type="Document" URL="../README.md"/>
		<Item Name="Release_Notes.txt" Type="Document" URL="../Release_Notes.txt"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="1D String Array to Delimited String.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/1D String Array to Delimited String.vi"/>
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Acquire Input Data.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Acquire Input Data.vi"/>
				<Item Name="Acquire Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Acquire Semaphore.vi"/>
				<Item Name="AddNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/AddNamedSemaphorePrefix.vi"/>
				<Item Name="ALM_Clear_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Clear_UD_Alarm.vi"/>
				<Item Name="ALM_Error_Resolve.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Error_Resolve.vi"/>
				<Item Name="ALM_Get_Alarms.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_Alarms.vi"/>
				<Item Name="ALM_Get_User_Name.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Get_User_Name.vi"/>
				<Item Name="ALM_GetTagURLs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_GetTagURLs.vi"/>
				<Item Name="ALM_Set_UD_Alarm.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Alarm.vi"/>
				<Item Name="ALM_Set_UD_Event.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/ALM_Set_UD_Event.vi"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="Batch Msg.lvclass" Type="LVClass" URL="/&lt;vilib&gt;/ActorFramework/Batch Msg/Batch Msg.lvclass"/>
				<Item Name="BuildErrorSource.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/BuildErrorSource.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Check Whether Timeouted.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/Check Whether Timeouted.vi"/>
				<Item Name="citadel_ConvertDatabasePathToName.vi" Type="VI" URL="/&lt;vilib&gt;/citadel/citadel_ConvertDatabasePathToName.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="Close Input Device.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Close Input Device.vi"/>
				<Item Name="Close Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Close Registry Key.vi"/>
				<Item Name="closeJoystick.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeJoystick.vi"/>
				<Item Name="closeKeyboard.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeKeyboard.vi"/>
				<Item Name="closeMouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/closeMouse.vi"/>
				<Item Name="compatCalcOffset.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatCalcOffset.vi"/>
				<Item Name="compatFileDialog.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatFileDialog.vi"/>
				<Item Name="compatOpenFileOperation.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOpenFileOperation.vi"/>
				<Item Name="Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDT.llb/Compress Digital.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="CreateOrAddLibraryToParent.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToParent.vi"/>
				<Item Name="CreateOrAddLibraryToProject.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/Variable/CreateOrAddLibraryToProject.vi"/>
				<Item Name="CTL_defaultProcessName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/cittools/CTL_defaultProcessName.vi"/>
				<Item Name="Delimited String to 1D String Array.vi" Type="VI" URL="/&lt;vilib&gt;/AdvancedString/Delimited String to 1D String Array.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="Dflt Data Dir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Dflt Data Dir.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="dsc_PrefsPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/info/dsc_PrefsPath.vi"/>
				<Item Name="dscCommn.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/common/dscCommn.dll"/>
				<Item Name="dscProc.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/process/dscProc.dll"/>
				<Item Name="DTbl Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Compress Digital.vi"/>
				<Item Name="DTbl Digital Size.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Digital Size.vi"/>
				<Item Name="DTbl Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DTblOps.llb/DTbl Empty Digital.vi"/>
				<Item Name="DWDT Compress Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Compress Digital.vi"/>
				<Item Name="DWDT Empty Digital.vi" Type="VI" URL="/&lt;vilib&gt;/Waveform/DWDTOps.llb/DWDT Empty Digital.vi"/>
				<Item Name="ERR_ErrorClusterFromErrorCode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_ErrorClusterFromErrorCode.vi"/>
				<Item Name="ERR_GetErrText.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_GetErrText.vi"/>
				<Item Name="ERR_MergeErrors.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/error/ERR_MergeErrors.vi"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrorDescriptions.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/ErrorDescriptions.vi"/>
				<Item Name="errorList.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/errorList.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="FileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInfo.vi"/>
				<Item Name="FileVersionInformation.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FileVersionInformation.ctl"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="FindCloseTagByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindCloseTagByName.vi"/>
				<Item Name="FindElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElement.vi"/>
				<Item Name="FindElementStartByName.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindElementStartByName.vi"/>
				<Item Name="FindEmptyElement.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindEmptyElement.vi"/>
				<Item Name="FindFirstTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindFirstTag.vi"/>
				<Item Name="FindMatchingCloseTag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/FindMatchingCloseTag.vi"/>
				<Item Name="FixedFileInfo_Struct.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/FixedFileInfo_Struct.ctl"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get File Extension.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Get File Extension.vi"/>
				<Item Name="Get LV Class Default Value By Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value By Name.vi"/>
				<Item Name="Get LV Class Default Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Default Value.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetFileVersionInfo.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfo.vi"/>
				<Item Name="GetFileVersionInfoSize.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/GetFileVersionInfoSize.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/GetNamedSemaphorePrefix.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="HIST_FormatTagname&amp;ProcessFilterSpec.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/historical/internal/alarm/HIST_FormatTagname&amp;ProcessFilterSpec.vi"/>
				<Item Name="Initialize Mouse.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/Initialize Mouse.vi"/>
				<Item Name="joystickAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/joystickAcquire.vi"/>
				<Item Name="keyboardAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/keyboardAcquire.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVDateTimeRec.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVDateTimeRec.ctl"/>
				<Item Name="LVOffsetAndMultiplierTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVOffsetAndMultiplierTypeDef.ctl"/>
				<Item Name="LVPoint32TypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVPoint32TypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="LVRowAndColumnTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRowAndColumnTypeDef.ctl"/>
				<Item Name="mouseAcquire.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/inputDevices.llb/mouseAcquire.vi"/>
				<Item Name="MoveMemory.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/MoveMemory.vi"/>
				<Item Name="NET_GetHostName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_GetHostName.vi"/>
				<Item Name="NET_IsComputerLocalhost.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_IsComputerLocalhost.vi"/>
				<Item Name="NET_localhostToMachineName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_localhostToMachineName.vi"/>
				<Item Name="NET_resolveNVIORef.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_resolveNVIORef.vi"/>
				<Item Name="NET_SameMachine.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_SameMachine.vi"/>
				<Item Name="NET_tagURLdecode.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/net/NET_tagURLdecode.vi"/>
				<Item Name="NI_AALBase.lvlib" Type="Library" URL="/&lt;vilib&gt;/Analysis/NI_AALBase.lvlib"/>
				<Item Name="ni_citadel_lv.dll" Type="Document" URL="/&lt;vilib&gt;/citadel/ni_citadel_lv.dll"/>
				<Item Name="NI_Data Type.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/Data Type/NI_Data Type.lvlib"/>
				<Item Name="NI_DSC.lvlib" Type="Library" URL="/&lt;vilib&gt;/lvdsc/NI_DSC.lvlib"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_Gmath.lvlib" Type="Library" URL="/&lt;vilib&gt;/gmath/NI_Gmath.lvlib"/>
				<Item Name="ni_logos_BuildURL.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_BuildURL.vi"/>
				<Item Name="ni_logos_ValidatePSPItemName.vi" Type="VI" URL="/&lt;vilib&gt;/variable/logos/dll/ni_logos_ValidatePSPItemName.vi"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_Security Domain.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Domain.ctl"/>
				<Item Name="NI_Security Get Domains.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Get Domains.vi"/>
				<Item Name="NI_Security Identifier.ctl" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Identifier.ctl"/>
				<Item Name="NI_Security Resolve Domain.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/security/user/NI_Security Resolve Domain.vi"/>
				<Item Name="NI_Security_GetTimeout.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_GetTimeout.vi"/>
				<Item Name="NI_Security_ProgrammaticLogin.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ProgrammaticLogin.vi"/>
				<Item Name="NI_Security_ResolveDomainID.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainID.vi"/>
				<Item Name="NI_Security_ResolveDomainName.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/security/internal/NI_Security_ResolveDomainName.vi"/>
				<Item Name="ni_security_salapi.dll" Type="Document" URL="/&lt;vilib&gt;/Platform/security/ni_security_salapi.dll"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="ni_tagger_lv_NewFolder.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_NewFolder.vi"/>
				<Item Name="ni_tagger_lv_ReadVariableConfig.vi" Type="VI" URL="/&lt;vilib&gt;/variable/tagger/ni_tagger_lv_ReadVariableConfig.vi"/>
				<Item Name="NI_Variable.lvlib" Type="Library" URL="/&lt;vilib&gt;/variable/NI_Variable.lvlib"/>
				<Item Name="nialarms.dll" Type="Document" URL="/&lt;vilib&gt;/lvdsc/alarm/internal/nialarms.dll"/>
				<Item Name="Not A Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Not A Semaphore.vi"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Obtain Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Obtain Semaphore Reference.vi"/>
				<Item Name="Open Registry Key.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Open Registry Key.vi"/>
				<Item Name="Open_Create_Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/Open_Create_Replace File.vi"/>
				<Item Name="ParseXMLFragments.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/ParseXMLFragments.vi"/>
				<Item Name="PRC_AdoptVarBindURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_AdoptVarBindURL.vi"/>
				<Item Name="PRC_CachedLibVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CachedLibVariables.vi"/>
				<Item Name="PRC_CommitMultiple.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CommitMultiple.vi"/>
				<Item Name="PRC_ConvertDBAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ConvertDBAttr.vi"/>
				<Item Name="PRC_CreateFolders.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateFolders.vi"/>
				<Item Name="PRC_CreateProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateProc.vi"/>
				<Item Name="PRC_CreateSubLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateSubLib.vi"/>
				<Item Name="PRC_CreateVar.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_CreateVar.vi"/>
				<Item Name="PRC_DataType2Prototype.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DataType2Prototype.vi"/>
				<Item Name="PRC_DeleteLibraryItems.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteLibraryItems.vi"/>
				<Item Name="PRC_DeleteProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DeleteProc.vi"/>
				<Item Name="PRC_Deploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Deploy.vi"/>
				<Item Name="PRC_DumpProcess.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpProcess.vi"/>
				<Item Name="PRC_DumpSharedVariables.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_DumpSharedVariables.vi"/>
				<Item Name="PRC_EnableAlarmLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableAlarmLogging.vi"/>
				<Item Name="PRC_EnableDataLogging.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_EnableDataLogging.vi"/>
				<Item Name="PRC_GetLibFromURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetLibFromURL.vi"/>
				<Item Name="PRC_GetMonadAttr.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadAttr.vi"/>
				<Item Name="PRC_GetMonadList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetMonadList.vi"/>
				<Item Name="PRC_GetProcList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcList.vi"/>
				<Item Name="PRC_GetProcSettings.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetProcSettings.vi"/>
				<Item Name="PRC_GetVarAndSubLibs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarAndSubLibs.vi"/>
				<Item Name="PRC_GetVarList.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GetVarList.vi"/>
				<Item Name="PRC_GroupSVs.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_GroupSVs.vi"/>
				<Item Name="PRC_IOServersToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_IOServersToLib.vi"/>
				<Item Name="PRC_MakeFullPathWithCurrentVIsCallerPath.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MakeFullPathWithCurrentVIsCallerPath.vi"/>
				<Item Name="PRC_MutipleDeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_MutipleDeploy.vi"/>
				<Item Name="PRC_OpenOrCreateLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_OpenOrCreateLib.vi"/>
				<Item Name="PRC_ParseLogosURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ParseLogosURL.vi"/>
				<Item Name="PRC_ROSProc.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_ROSProc.vi"/>
				<Item Name="PRC_SVsToLib.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_SVsToLib.vi"/>
				<Item Name="PRC_Undeploy.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/process/internal/PRC_Undeploy.vi"/>
				<Item Name="PSP Enumerate Network Items.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/tagapi/internal/PSP Enumerate Network Items.vi"/>
				<Item Name="PTH_ConstructCustomURL.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/common/path/PTH_ConstructCustomURL.vi"/>
				<Item Name="Read From XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(array).vi"/>
				<Item Name="Read From XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File(string).vi"/>
				<Item Name="Read From XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Read From XML File.vi"/>
				<Item Name="Read Registry Value DWORD.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value DWORD.vi"/>
				<Item Name="Read Registry Value Simple STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple STR.vi"/>
				<Item Name="Read Registry Value Simple U32.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple U32.vi"/>
				<Item Name="Read Registry Value Simple.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value Simple.vi"/>
				<Item Name="Read Registry Value STR.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value STR.vi"/>
				<Item Name="Read Registry Value.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Read Registry Value.vi"/>
				<Item Name="Registry Handle Master.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Handle Master.vi"/>
				<Item Name="Registry refnum.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry refnum.ctl"/>
				<Item Name="Registry RtKey.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry RtKey.ctl"/>
				<Item Name="Registry SAM.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry SAM.ctl"/>
				<Item Name="Registry Simplify Data Type.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry Simplify Data Type.vi"/>
				<Item Name="Registry View.ctl" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry View.ctl"/>
				<Item Name="Registry WinErr-LVErr.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/Registry WinErr-LVErr.vi"/>
				<Item Name="Release Semaphore Reference.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore Reference.vi"/>
				<Item Name="Release Semaphore.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Release Semaphore.vi"/>
				<Item Name="RemoveNamedSemaphorePrefix.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/RemoveNamedSemaphorePrefix.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Select Event Type.ctl" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/Select Event Type.ctl"/>
				<Item Name="Semaphore RefNum" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore RefNum"/>
				<Item Name="Semaphore Refnum Core.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Semaphore Refnum Core.ctl"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Busy.vi"/>
				<Item Name="Set Cursor (Cursor ID).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Cursor ID).vi"/>
				<Item Name="Set Cursor (Icon Pict).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor (Icon Pict).vi"/>
				<Item Name="Set Cursor.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Set Cursor.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="STR_ASCII-Unicode.vi" Type="VI" URL="/&lt;vilib&gt;/registry/registry.llb/STR_ASCII-Unicode.vi"/>
				<Item Name="subFile Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/express/express input/FileDialogBlock.llb/subFile Dialog.vi"/>
				<Item Name="Subscribe All Local Processes.vi" Type="VI" URL="/&lt;vilib&gt;/lvdsc/controls/Alarms and Events/internal/Subscribe All Local Processes.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Time-Delayed Send Message Core.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message Core.vi"/>
				<Item Name="Time-Delayed Send Message.vi" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delayed Send Message.vi"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="Unset Busy.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/cursorutil.llb/Unset Busy.vi"/>
				<Item Name="usereventprio.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/usereventprio.ctl"/>
				<Item Name="Validate Semaphore Size.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/semaphor.llb/Validate Semaphore Size.vi"/>
				<Item Name="VariantType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/VariantDataType/VariantType.lvlib"/>
				<Item Name="VerQueryValue.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/fileVersionInfo.llb/VerQueryValue.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Write Spreadsheet String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write Spreadsheet String.vi"/>
				<Item Name="Write To Spreadsheet File (DBL).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (DBL).vi"/>
				<Item Name="Write To Spreadsheet File (I64).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (I64).vi"/>
				<Item Name="Write To Spreadsheet File (string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File (string).vi"/>
				<Item Name="Write To Spreadsheet File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Write To Spreadsheet File.vi"/>
				<Item Name="Write to XML File(array).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(array).vi"/>
				<Item Name="Write to XML File(string).vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File(string).vi"/>
				<Item Name="Write to XML File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/xml.llb/Write to XML File.vi"/>
			</Item>
			<Item Name="Advapi32.dll" Type="Document" URL="Advapi32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="CSPP_DMM.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DMM/CSPP_DMM.lvlib"/>
			<Item Name="CSPP_DMMGui.lvlib" Type="Library" URL="../Packages/CSPP_DeviceBase/CSPP_DMMGui/CSPP_DMMGui.lvlib"/>
			<Item Name="CSPP_PVSubscriber.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_PVSubscriber/CSPP_PVSubscriber.lvlib"/>
			<Item Name="CSPP_Watchdog Msg.lvlib" Type="Library" URL="../Packages/CSPP_Core/Messages/CSPP_Watchdog Msg/CSPP_Watchdog Msg.lvlib"/>
			<Item Name="CSPP_Watchdog.lvlib" Type="Library" URL="../Packages/CSPP_Core/Actors/CSPP_Watchdog/CSPP_Watchdog.lvlib"/>
			<Item Name="kernel32.dll" Type="Document" URL="kernel32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="lksock.dll" Type="Document" URL="lksock.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="logosbrw.dll" Type="Document" URL="/&lt;resource&gt;/logosbrw.dll"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="lvanlys.dll" Type="Document" URL="/&lt;resource&gt;/lvanlys.dll"/>
			<Item Name="lvinput.dll" Type="Document" URL="/&lt;resource&gt;/lvinput.dll"/>
			<Item Name="NI-FPGA-PPG.lvproj_PCI-7811R_FPGA_target_main.vi.lvbitx" Type="Document" URL="../instr.lib/NI-FPGA-PPG/FPGA Bitfiles/NI-FPGA-PPG.lvproj_PCI-7811R_FPGA_target_main.vi.lvbitx"/>
			<Item Name="NI-FPGA-PPG.lvproj_PXI-7841R_FPGA_target_main.vi.lvbitx" Type="Document" URL="../instr.lib/NI-FPGA-PPG/FPGA Bitfiles/NI-FPGA-PPG.lvproj_PXI-7841R_FPGA_target_main.vi.lvbitx"/>
			<Item Name="NiFpgaLv.dll" Type="Document" URL="NiFpgaLv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="nitaglv.dll" Type="Document" URL="nitaglv.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="NVIORef.dll" Type="Document" URL="NVIORef.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="SCT Default Types.ctl" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Default Types.ctl"/>
			<Item Name="SCT Get LVRTPath.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get LVRTPath.vi"/>
			<Item Name="SCT Get Types.vi" Type="VI" URL="/&lt;resource&gt;/dialog/variable/SCT Get Types.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="target.state.ctl" Type="VI" URL="../instr.lib/NI-FPGA-MCS/target/target.state.ctl"/>
			<Item Name="User32.dll" Type="Document" URL="User32.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
			<Item Name="UtilityLib.obtain library name and version.vi" Type="VI" URL="../CSlibraries/UtilityLib/UtilityLib.obtain library name and version.vi"/>
			<Item Name="version.dll" Type="Document" URL="version.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="CSPP-App" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{773E39E1-BBD6-4173-9460-6FE7C7522091}</Property>
				<Property Name="App_INI_GUID" Type="Str">{A41324B4-1D83-44D0-AB6D-155F8178B03C}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/CSPP.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{95E968AE-FBA3-4AF4-B59D-842A4FF56C3A}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-App</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{6211658F-9962-4EFC-81EE-AF45E75227E4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">16</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App/CSPP.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_Vardep[0].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[0].LibItemID" Type="Ref"></Property>
				<Property Name="Exe_VardepDeployAtStartup" Type="Bool">true</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepLibItemCount" Type="Int">1</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{CA5CBA29-3E1E-424E-BC39-332477E59039}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref"></Property>
				<Property Name="Source[4].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[4].type" Type="Str">Library</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">Include</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{20017911-8940-4421-B57F-6FCEEB0905B6}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP.exe</Property>
			</Item>
			<Item Name="CSPP-AppExe" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{5E268D53-3723-4001-92D7-769A3F2CC9C8}</Property>
				<Property Name="App_INI_GUID" Type="Str">{58BE6F3B-2630-4FAF-B729-42C92FCB4801}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/CSPP.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{CCDF2293-27D0-40B1-8DB3-7181D7B2947B}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP-AppExe</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{C14B872D-BF54-41F0-AF2B-A3B09DA3EC5B}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">92</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App/CSPP.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/NI_AB_PROJECTNAME/CSPP-App/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_Vardep[0].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_Vardep[1].LibDeploy" Type="Bool">true</Property>
				<Property Name="Exe_VardepDeployAtStartup" Type="Bool">true</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepLibItemCount" Type="Int">2</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{833DA295-5F88-488D-B9CD-C8C74C840994}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[5].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[7].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[8].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/CSPP_MainExe.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2017 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP-App</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{C57B042B-41E3-4B31-AB68-4A375C745B16}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP.exe</Property>
			</Item>
			<Item Name="CSPP_Genesys" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{54A25A14-E337-4664-83F1-73EC6D6AD7D4}</Property>
				<Property Name="App_INI_GUID" Type="Str">{FAF1F7B4-408A-41D4-9C7C-7789A9203172}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_Genesys.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.certificate" Type="Str">Neidherr Dennis</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{9DB9BF4B-260A-47ED-A67D-083B6AE7B61C}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Executable only with Genesys PS (no DSC)</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP_Genesys</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/CSPP_Genesys</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{7605C61B-E166-4C98-AD02-4F0E0CDF16AC}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">20</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP_Genesys.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CSPP_Genesys/CSPP_Genesys.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/CSPP_Genesys/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{BEDD005A-71F9-4CD8-97AC-82AFD0AED0E2}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_PPG.ini</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib</Property>
				<Property Name="Source[13].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[13].type" Type="Str">Library</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_HVSwitch2.ini</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_Genesys.ini</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/CSPP_MainExe.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">16</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP_PPG (no DSC); Only GSI-HVSwitch2</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP_HVSwitch2</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP_HVSwitch2</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{7AC4CFE2-A83C-4A17-88F3-732F86486F1B}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP_Genesys.exe</Property>
			</Item>
			<Item Name="CSPP_HVSwitch2" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{481D6F96-1DEB-4DF0-97E1-9B7E575C17F1}</Property>
				<Property Name="App_INI_GUID" Type="Str">{7504D464-EF41-45FF-975E-76020D524E13}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_HVSwitch2.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.certificate" Type="Str">Neidherr Dennis</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{8B7DBCFB-5845-49C4-B7AA-E7F4C23D2D66}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Executable only with GSI-HVSwitch (no DSC)</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP_HVSwitch2</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/CSPP_HVSwitch2</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{61F2A733-FED6-4096-A023-0329D74156EA}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">17</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP_HVSwitch2.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CSPP_HVSwitch2/CSPP_HVSwitch2.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/CSPP_HVSwitch2/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{156C4800-6FC5-46AA-8252-D66BF329E971}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_PPG.ini</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib</Property>
				<Property Name="Source[13].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[13].type" Type="Str">Library</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_HVSwitch2.ini</Property>
				<Property Name="Source[14].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/CSPP_MainExe.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">15</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP_PPG (no DSC); Only GSI-HVSwitch2</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP_HVSwitch2</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP_HVSwitch2</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{3DB0ADD9-FB0C-4718-AFC7-B799D11AB621}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP_HVSwitch2.exe</Property>
			</Item>
			<Item Name="CSPP_ISEG" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{C8AC8855-0C64-40E2-9047-2E4B5BD4C598}</Property>
				<Property Name="App_INI_GUID" Type="Str">{00D34311-9B12-4F60-9707-E787E3748DD5}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_ISEG.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.certificate" Type="Str">Neidherr Dennis</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{5B7308FC-D776-48B7-B3A8-EBC0A30D39D9}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Executable with ISEG and PSConnect (with DSC)</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP_ISEG</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/CSPP_ISEG</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{731C3D41-D075-43C0-AB73-AF4CA61E05B4}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">21</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP_ISEG.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CSPP_ISEG/CSPP_ISEG.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/CSPP_ISEG/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{B51D6EB2-604D-435C-AD95-D2834C02AABB}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_PPG.ini</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/_Applikation SV-Files/myGSI-HVSwitch2.lvlib</Property>
				<Property Name="Source[13].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[13].type" Type="Str">Library</Property>
				<Property Name="Source[14].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[14].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_HVSwitch2.ini</Property>
				<Property Name="Source[15].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[15].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_ISEG.ini</Property>
				<Property Name="Source[15].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/CSPP_MainExe.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">16</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">ISEG + PSConnect (with DSC)</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP_ISEG</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP_ISEG</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{EB45E818-5CFE-4D94-B927-A698DC0FA53A}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP_ISEG.exe</Property>
			</Item>
			<Item Name="CSPP_PPG" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{5DC6D122-FB9A-46EA-A570-AB7E24B7E421}</Property>
				<Property Name="App_INI_GUID" Type="Str">{CF3DA987-45F6-40B1-AF2E-D8819BD2FE34}</Property>
				<Property Name="App_INI_itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_PPG.ini</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="App_winsec.certificate" Type="Str">Neidherr Dennis</Property>
				<Property Name="App_winsec.timestamp" Type="Str">http://timestamp.digicert.com</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{D8564FAA-6144-41A4-832D-FDAB25EBDE73}</Property>
				<Property Name="Bld_buildSpecDescription" Type="Str">Executable only with FPGA-PPG (no DSC)</Property>
				<Property Name="Bld_buildSpecName" Type="Str">CSPP_PPG</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">../builds/CSPP_PPG</Property>
				<Property Name="Bld_localDestDirType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{B0FE79DE-292B-4BD6-9DAC-A85B24320076}</Property>
				<Property Name="Bld_supportedLanguage[0]" Type="Str">English</Property>
				<Property Name="Bld_supportedLanguageCount" Type="Int">1</Property>
				<Property Name="Bld_userLogFile" Type="Path">../builds/CSPP-App_log.txt</Property>
				<Property Name="Bld_userLogFile.pathType" Type="Str">relativeToCommon</Property>
				<Property Name="Bld_version.build" Type="Int">20</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">CSPP_PPG.exe</Property>
				<Property Name="Destination[0].path" Type="Path">../builds/CSPP_PPG/CSPP_PPG.exe</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Support Directory</Property>
				<Property Name="Destination[1].path" Type="Path">../builds/CSPP_PPG/data</Property>
				<Property Name="DestinationCount" Type="Int">2</Property>
				<Property Name="Exe_cmdLineArgs" Type="Bool">true</Property>
				<Property Name="Exe_iconItemID" Type="Ref">/My Computer/CSPP.ico</Property>
				<Property Name="Exe_VardepHideDeployDlg" Type="Bool">true</Property>
				<Property Name="Exe_VardepUndeployOnExit" Type="Bool">true</Property>
				<Property Name="Source[0].itemID" Type="Str">{156C4800-6FC5-46AA-8252-D66BF329E971}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/CSPP_Main.vi</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[10].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[10].itemID" Type="Ref">/My Computer/_Applikation Ini-Files/CSPP_PPG.ini</Property>
				<Property Name="Source[10].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[11].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[11].itemID" Type="Ref">/My Computer/Packages/ObjectManager/CSPP_ObjectManager_SV.lvlib</Property>
				<Property Name="Source[11].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[11].type" Type="Str">Library</Property>
				<Property Name="Source[12].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[12].itemID" Type="Ref">/My Computer/Packages/Core/CSPP_Core_SV.lvlib</Property>
				<Property Name="Source[12].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[12].type" Type="Str">Library</Property>
				<Property Name="Source[13].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[13].itemID" Type="Ref">/My Computer/_Applikation SV-Files/myCSPP_FPGA_SV.lvlib</Property>
				<Property Name="Source[13].Library.allowMissingMembers" Type="Bool">true</Property>
				<Property Name="Source[13].type" Type="Str">Library</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/Release_Notes.txt</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Change_Log.txt</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/README.md</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64.dll</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x64_32.dll</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/hptdc_driver_3.7.0_x86.dll</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/instr.lib/TDC8HP.lvlib/DLL/TDC8HP_LabView10.dll</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/CSPP_MainExe.vi</Property>
				<Property Name="Source[9].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[9].type" Type="Str">VI</Property>
				<Property Name="SourceCount" Type="Int">14</Property>
				<Property Name="TgtF_companyName" Type="Str">GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">CSPP_PPG (no DSC); Using Standard Cycle and Standard In/Out of 7811R</Property>
				<Property Name="TgtF_internalName" Type="Str">CSPP_PPG</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2020 GSI Helmholtzzentrum für Schwerionenforschung GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">CSPP_PPG</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{9A08EAD9-C994-4218-8039-C1D587C9F2BD}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">CSPP_PPG.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
